/*
 * FILNAVN :    Balance.ino
 *
 * BESKRIVELSE : 
 *      Står for PID loopet og styringen af stepper motorer
 *
 * NOTER :
 *      En stor del af metoderne i denne kode er lånt af Joop Brokking, dette gælder
 *              - Vinkel aflæsning
 *              - PID udregning
 *              - Motor puls udregning
 *              - Motor puls timing
 *      Kilde: http://www.brokking.net/yabr_downloads.html
 *
 * FORFATTER : 
 *      Gustav, Julian, Laurits, Victor
 */
#include <Wire.h>

//Variabler for motor styring
int left_counter, left_time, left_throttle = 0;
int right_counter, right_time, right_throttle = 0;

//Accel variabler. TODO Burde være i GyroAccel.h
float accel_cal_value = 350;                            //Enter the accelerometer calibration value
float gyro_angle = 0;                     // Grader per sekundt. 

//Andre variabler
bool running = false;                           //Beskriver om robotten skal køre, bliver tændt når robotten står lige. 
int loopTime = 4000;                            //Bestemmer tiden mellem loopsne
unsigned long long loop_timer = 0;              //Skal ikke jysteres
int remoteMax = 99;                             //Den maximale værdi som PID værdier kommer i

//Variabler til drejning
float turn = 0;
float turnTarget = 0;
float turnValue = 80;                           //Hvor meget den drejer
float turnAccel = 0.1;                          //Dreje acceleation

//Variabler til kørsel
float pid_setPoint = 0;
float pid_setPointTarget = 0;
float pid_setPointAccel = 0.01;                 //Accelerationen
float pid_setPointValue = 3;                    //Køre hastigheden

//PID settings 
float pid_p = 60;
int pid_p_max = 100;

float pid_i = 1.5;
float pid_i_out = 0;                      // Dette hører til måden intergralet er implementeret
int pid_i_max = 10;

float pid_d = 60;
float pid_error_prev = 0;
int pid_d_max = 100;

float pid_out = 0;
int pid_max = 400; 


//Gyro stuff
float gyro_pitch_calibration_value = 0;
float gyro_yaw_calibration_value = 0;

int gyro_address = 0x68;

int GyroReg = 0x43;
int AccelReg = 0x3B;



void setup()
{
    Serial.begin(115200);
    Serial.println("Start");
    Wire.begin();

                                                   //Configure digital poort 6 as output

    //Set I2C frekvens til 400kHz. 
    TWBR = 12;

    //Sæt Registerne til 0
    TCCR2A = 0;
    TCCR2B = 0;
    //Aktiver CMP
    TIMSK2 |= (1 << OCIE2A);
    //Sæt prescaler til 8
    TCCR2B |= (1 << CS21);
    //Denne værdi bliver udregnet med værdierne i raporten
    OCR2A = 39;
    //Aktiver CTC mode
    TCCR2A |= (1 << WGM21);

  
    pinMode(2, OUTPUT);
    pinMode(3, OUTPUT);
    pinMode(4, OUTPUT);
    pinMode(5, OUTPUT);
    pinMode(13, OUTPUT);     
    pinMode(6, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);
    pinMode(9, OUTPUT);
    pinMode(10, OUTPUT);

    digitalWrite(7, HIGH);
    digitalWrite(8, HIGH);
    digitalWrite(9, HIGH);
    digitalWrite(10, HIGH);
    
    
    //Det følgende opsætningsstykke er taget fra Joop Brokking
    //Sæt accelerometer til full range 4G
    Wire.beginTransmission(gyro_address);
    Wire.write(0x1C);
    Wire.write(0x08);
    Wire.endTransmission();
    //Tænd output filtering til omkring 43Hz
    Wire.beginTransmission(gyro_address);
    Wire.write(0x1A);
    Wire.write(0x03);
    Wire.endTransmission();

    //Som standart sover modulet, men kan tændes ved at sætte PWR_MGMT_1 til 0
    Wire.beginTransmission(gyro_address);
    Wire.write(0x6B);
    Wire.write(0x00);
    Wire.endTransmission();
    //Sæt gyro rækkevidde til 250 grader per sekundt 
    Wire.beginTransmission(gyro_address);
    Wire.write(0x1B);
    Wire.write(0x00);
    Wire.endTransmission();
    
    //Calibrer accelerometer over 500 målinger
    float calVal = 0; 
    for(int i = 0; i < 500; i++){
        Wire.beginTransmission(0x68);
        Wire.write(AccelReg);
        Wire.endTransmission();
        Wire.requestFrom(0x68,2);
        calVal += (Wire.read()<<8|Wire.read())*-1;
    }
    //Gennemsnittet over målingerne kan findes ved at dividere
    calVal /= 500;
    accel_cal_value = calVal;


    //Denne funktion vil ændre de to overstående variabler som er defineret i gyroaccel.h
    for(int receive_counter = 0; receive_counter < 500; receive_counter++){
        Wire.beginTransmission(gyro_address);
        Wire.write(GyroReg);
        Wire.endTransmission();
        Wire.requestFrom(gyro_address, 4);
        gyro_yaw_calibration_value += Wire.read()<<8|Wire.read();
        gyro_pitch_calibration_value += Wire.read()<<8|Wire.read();
        delay(3);
    }

    gyro_pitch_calibration_value /= 500;
    gyro_yaw_calibration_value /= 500;    



    //Dette vil få loop() til at køre med ved en konstant frekvens. Hvilket er vigtigt når man måler vinklen.
    loop_timer = micros();
}

//Gør det rigtige med den indkommende karakter
char pid_char = 0;
void handleInput(char c){
    //Hvis denne er sand skal der modtages en PID værdi
    if(pid_char != 0){
        switch(pid_char){
            case 'p':
                //PID værdien kommer mellem 0 og remoteMax, men bliver mappet til noget mere passende.
                pid_p = mapfloat(c, 0, remoteMax, 0, pid_p_max);
                break;
            case 'i':
                pid_i = mapfloat(c, 0, remoteMax, 0, pid_i_max);
                break;
            case 'd':
                pid_d = mapfloat(c, 0, remoteMax, 0, pid_d_max);
                break;
        }
        //Sæt charakter tilbage til 0, så den forventer en kommando
        pid_char = 0;
        return;
    }
    switch(c){
        case 'p':
        case 'i':
        case 'd':
            //Vil blive køre hvis c er enten P, I eller D. Hvilket betyder at den skal modtage en extra værdi
            pid_char = c;
            break;
        case 'f':
            //Frem      
            pid_setPointTarget = pid_setPointValue;
            break;
        case 't':
            //Tilbage
            pid_setPointTarget = -1*pid_setPointValue;
            break;
        case 's':
            //Stop, altså kør ikke frem eller tilbage
            pid_setPointTarget = 0; 
            break;
        case 'h':
            //Højre
            turnTarget = turnValue;
            break;
        case 'v':
            //Venstre
            turnTarget = turnValue*-1;
            break;
        case 'l':
            //Lige
            turnTarget = 0;
            
    }
}

void loop()
{
    //Hvis der ikke er gået 4 millisekundter siden sidste puls, så string over. 
    if(micros() - loop_timer < loopTime){
        return;
    }
    
    //Indstild tidsvariablen
    loop_timer = micros();

    //Læs fra serial
    if(Serial.available()){
        char c = Serial.read();

        handleInput(c);
    }

    //Jyster testpoint
    pid_setPoint += (pid_setPointTarget - pid_setPoint)*pid_setPointAccel;
    turn += (turnTarget-turn)*turnAccel;

    //For at undgå vibrationer bruges gyroen i stedet for accel, problemet er at den kommer med en delta. Derfor måder man accel værdien når den starter og gemmer den i angle, og fra deraf bruger man gyro.
    
    //Læs to bytes fra ACCEL_ZOUT_H og ACCEL_ZOUT_L
    sendAndRequest(AccelReg, 2);  
    //Læs de bytes og dem i en int. Dette gøres ved at gemme de 8 første, rykke dem til siden også gem de sidste.
    int accel_raw_data = Wire.read() << 8 | Wire.read();         
    //Tilføj calibreringsværdi
    accel_raw_data += accel_cal_value; 

    //For at undgå dividering med 0, skal accel_raw_data være mellem +/-8192
    if(accel_raw_data > 8192)
        accel_raw_data = 8192;
    if(accel_raw_data < -8192)
        accel_raw_data = -8192;
    
    //Regn vinklen ud ved metoden beskrevet i rapporten. Asin funktionen er i radianer så der regnes om til grader. 
    float accel_angle = asin( (float)accel_raw_data / 8192.0) * 57.296;         
    

    //Start når den står lige
    if(!running && accel_angle > -0.5 && accel_angle < 0.5){
        gyro_angle = accel_angle;                                  
        running = true;                                           //Start the engines ;-D
        digitalWrite(6, HIGH);
    }


    //Læs fra gyro_xout og gyro_yout
    sendAndRequest(GyroReg, 4);

    //Læs fire bytes fra accell
    int gyro_yaw_raw_data = Wire.read()<<8|Wire.read();                       
    int gyro_pitch_raw_data = Wire.read()<<8|Wire.read();                    

    //Tilføj calibration value
    gyro_pitch_raw_data -= gyro_pitch_calibration_value;                
    gyro_yaw_raw_data -= gyro_yaw_calibration_value;


    //Dataen fra accel kommer i grader per sekundt, men denne funktion kører ved en anden hastighed. 
    gyro_angle += gyro_pitch_raw_data * 0.000030;


    //For at undgå gyro uprecision bliver værdien lige så stille korigeret. Forholdende kan ændres efter behov. 
    gyro_angle = gyro_angle * 0.9994 + accel_angle * 0.0006;


    //////////////////////////////////////////////////////////////////////////////////////////
    //PID udregninger 
    //////////////////////////////////////////////////////////////////////////////////////////
    float pid_error = gyro_angle - pid_setPoint;
    //P delen
    float pid_p_out = pid_p * pid_error;


    //Robotten skal helst læne sig i den retning den kører.
    if(pid_out> 5 || pid_out < -5)
        pid_error += pid_out * 0.015;

    //Regn intergrale delen
    pid_i_out += pid_i * pid_error;

    //I delen skal være mellem -pid_max og pid_max.
    if(pid_i_out > pid_max){
        pid_i_out = pid_max;
    }else if(pid_i_out < pid_max * -1){
        pid_i_out = pid_max * -1;
    }

    // D del
    float pid_d_out = (pid_error - pid_error_prev) * pid_d;

    //Gem error til næste cyklus
    pid_error_prev = pid_error;

    
    //Delene adderes og output checkes
    pid_out = pid_i_out + pid_p_out + pid_d_out;
    if(pid_out > pid_max){
        pid_out = pid_max;
    }else if(pid_out < pid_max * -1){
        pid_out = pid_max * -1;
    }

    //Gem error til næste
    pid_error_prev = pid_error;
    
    //For at den ikke står og ryster bliver out sat til 0 hvis den er mellem -5 og 5.

    if(pid_out < 5 && pid_out > -5){
        pid_out = 0;
    }

    //Impelenter dette bedre 
    if((gyro_angle > 30 || gyro_angle < -30) && running ){    //If the robot tips over or the start variable is zero or the battery is empty
        pid_out= 0;                                                         //Set the PID controller output to 0 so the motors stop moving
        pid_i_out = 0;                                                          //Reset the I-controller memory
        running = 0;                                                              //Set the start variable to 0
    }
    
    //
    //Turn management
    //
    

    //Fordel pidout
    float pid_out_right = pid_out + turn;
    float pid_out_left = pid_out + (-1*turn);

    //////////////////////////////////////////////////////
    //Motor compensation. Bruger Joop metode
    //////////////////////////////////////////////////////

    
    //Linealiser pid_out sådan at den giver en lineær ændring af motorerne. Credit til Joop Brokking for udregninger
    if(pid_out_left > 0){
        pid_out_left = 405 - (1/(pid_out_left + 9)) * 5500;
    }else if(pid_out_left < 0){
        pid_out_left = -405 - (1/(pid_out_left - 9)) * 5500;
    }

    if(pid_out_right > 0){
        pid_out_right = 405 - (1/(pid_out_right + 9)) * 5500;
    }else if(pid_out_right < 0){
        pid_out_right = -405 - (1/(pid_out_right - 9)) * 5500;
    }

    //Sæt motorstyringsvariablerne der vil blive brugt i stepper timer stykket. 
    if(pid_out_left > 0){
        left_throttle = 400 - pid_out_left;
    }else if(pid_out_left < 0){
        left_throttle = -400 - pid_out_left;
    }else {
        left_throttle = 0;
    }

    if(pid_out_right > 0){
        right_throttle = 400 - pid_out_right;
    }else if(pid_out_right < 0){
        right_throttle = -400 - pid_out_right;
    }else {
        right_throttle = 0;
    }

}

void sendAndRequest(int regis, int bytes)
{
    Wire.beginTransmission(gyro_address);
    Wire.write(regis);
    Wire.endTransmission();
    Wire.requestFrom(gyro_address, bytes);
}

ISR(TIMER2_COMPA_vect)
{
    //Lad counter stige med 1
    left_counter++;
    //Check om en cyclus er færdig 
    if(left_counter > left_time){
        //Start en ny cyclus og sæt tiden til left_throttle
        left_counter = 0;
        left_time = left_throttle;

        //Hvis den skal køre baglens bliver dir pinnen sat.
        if(left_time < 0){
            PORTD |= 0b00000100;
            left_time *= -1;
        } else {
            PORTD &= 0b11111011;
        }
    } else if(left_counter == 1){
        //Sæt stepping høj ved starten af en cyclus
        PORTD |= 0b00001000;
    } else if(left_counter == 2){
        //Ved næste TIMER2 trigger skal den sættes til lav igen.
        PORTD &= 0b11110111;
    }

    //Gør det samme ved den anden side
    right_counter++;
    if(right_counter > right_time){           
        right_counter = 0;
        right_time = right_throttle;
        if(right_time < 0){
            PORTD &= 0b11011111;
            right_time *= -1;
        } else {
            PORTD |= 0b00100000;
        }
    } else if(right_counter == 1){
        PORTD |= 0b00010000;
    } else if(right_counter == 2){
        PORTD &= 0b11101111;
    }

}

float mapfloat(long x, long x_min, long x_max, long y_min, long y_max)
{
    return (float)(x - x_min) * (y_max - y_min) / (float)(x_max - x_min) + y_min; 
}
